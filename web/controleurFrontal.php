<?php

////////////////////
// Initialisation //
////////////////////
use TheFeed\Lib\Psr4AutoloaderClass;

require_once __DIR__ . '/../vendor/autoload.php';


/////////////
// Routage //
/////////////

TheFeed\Controleur\RouteurURL::traiterRequete();



