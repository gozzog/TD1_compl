<?php

namespace TheFeed\Controleur;

use TheFeed\Lib\MessageFlash;

class ControleurGenerique {

    protected static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        require __DIR__ . "/../vue/$cheminVue";
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected static function rediriger(string $route = "", array $params = []): void
    {
        // Get the service from the container
        $generateurUrl = $container->get('generateur_url');

        // Generate the absolute URL based on the route and parameters
        $url = $generateurUrl->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL);

        // Redirect to the absolute URL
        header("Location: $url");
        exit();
    }

    public static function afficherErreur($messageErreur = "", $controleur = ""): void
    {
        $messageErreurVue = "Problème";
        if ($controleur !== "")
            $messageErreurVue .= " avec le contrôleur $controleur";
        if ($messageErreur !== "")
            $messageErreurVue .= " : $messageErreur";

        ControleurGenerique::afficherVue('vueGenerale.php', [
            "pagetitle" => "Problème",
            "cheminVueBody" => "erreur.php",
            "errorMessage" => $messageErreurVue
        ]);
    }

}
