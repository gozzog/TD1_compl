<?php

namespace TheFeed\Modele\Repository;

use TheFeed\Modele\DataObject\Utilisateur;
use PDOStatement;

class UtilisateurRepository
{

    /**
     * @return Utilisateur[]
     */
    public function recuperer(): array
    {
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM utilisateurs");
        $statement->execute();

        $utilisateurs = [];

        foreach ($statement as $data) {
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($data["idutilisateur"]);
            $utilisateur->setLogin($data["login"]);
            $utilisateur->setMdpHache($data["mdphache"]);
            $utilisateur->setEmail($data["email"]);
            $utilisateur->setNomPhotoDeProfil($data["nomphotodeprofil"]);
            $utilisateurs[] = $utilisateur;
        }

        return $utilisateurs;
    }

    public function recupererParClePrimaire($id): ?Utilisateur
    {
        $values = [
            "idutilisateur" => $id,
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM utilisateurs WHERE idutilisateur = :idutilisateur");
        return $this->extraireUtilisateur($statement, $values);
    }

    public function recupererParLogin($login)
    {
        $values = [
            "login" => $login,
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM utilisateurs WHERE login = :login");
        return $this->extraireUtilisateur($statement, $values);
    }

    public function recupererParEmail($email)
    {
        $values = [
            "email" => $email,
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT * FROM utilisateurs WHERE email = :email");
        return $this->extraireUtilisateur($statement, $values);
    }

    public function ajouter($entite)
    {
        $values = [
            "login" => $entite->getLogin(),
            "mdphache" => $entite->getMdpHache(),
            "email" => $entite->getEmail(),
            "nomphotodeprofil" => $entite->getNomPhotoDeProfil()
        ];
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $statement = $pdo->prepare("INSERT INTO utilisateurs (login, mdphache, email, nomphotodeprofil) VALUES(:login, :mdphache, :email, :nomphotodeprofil);");
        $statement->execute($values);
        return $pdo->lastInsertId();
    }

    public function mettreAJour($entite)
    {
        $values = [
            "idutilisateur" => $entite->getIdUtilisateur(),
            "login" => $entite->getLogin(),
            "mdphache" => $entite->getMdpHache(),
            "email" => $entite->getEmail(),
            "nomphotodeprofil" => $entite->getNomPhotoDeProfil()
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("UPDATE utilisateurs SET login = :login, mdphache = :mdphache, email = :email, nomphotodeprofil = :nomphotodeprofil WHERE idutilisateur = :idutilisateur;");
        $statement->execute($values);
    }

    public function supprimer($entite)
    {
        $values = [
            "idutilisateur" => $entite->getIdUtilisateur(),
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("DELETE FROM utilisateurs WHERE idutilisateur = :idutilisateur");
        $statement->execute($values);
    }

    /**
     * @param bool|PDOStatement $statement
     * @param array $values
     * @return Utilisateur|void
     */
    public function extraireUtilisateur(PDOStatement $statement, array $values)
    {
        $statement->execute($values);
        $data = $statement->fetch();
        if ($data) {
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($data["idutilisateur"]);
            $utilisateur->setLogin($data["login"]);
            $utilisateur->setMdpHache($data["mdphache"]);
            $utilisateur->setEmail($data["email"]);
            $utilisateur->setNomPhotoDeProfil($data["nomphotodeprofil"]);
            return $utilisateur;
        }
    }
}
