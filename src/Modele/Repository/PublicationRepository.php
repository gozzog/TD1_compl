<?php

namespace TheFeed\Modele\Repository;

use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\DataObject\Utilisateur;
use DateTime;

class PublicationRepository
{
    /**
     * @return Publication[]
     * @throws \Exception
     */
    public function recuperer(): array
    {
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT idpublication, message, date, idutilisateur, login, nomphotodeprofil
                                                FROM publications p
                                                JOIN utilisateurs u on p.idAuteur = u.idutilisateur
                                                ORDER BY date DESC");
        $statement->execute();

        $publications = [];

        foreach ($statement as $data) {
            $publication = new Publication();
            $publication->setIdPublication($data["idpublication"]);
            $publication->setMessage($data["message"]);
            $publication->setDate(new DateTime($data["date"]));
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($data["idutilisateur"]);
            $utilisateur->setLogin($data["login"]);
            $utilisateur->setNomPhotoDeProfil($data["nomphotodeprofil"]);
            $publication->setAuteur($utilisateur);
            $publications[] = $publication;
        }

        return $publications;
    }

    /**
     * @param $idutilisateur
     * @return Publication[]
     * @throws \Exception
     */
    public function recupererParAuteur($idutilisateur): array
    {
        $values = [
            "idAuteur" => $idutilisateur,
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT idpublication, message, date, idutilisateur, login, nomphotodeprofil
                                                FROM publications p
                                                JOIN utilisateurs u on p.idAuteur = u.idutilisateur
                                                WHERE idAuteur = :idAuteur
                                                ORDER BY date DESC");
        $statement->execute($values);

        $publis = [];

        foreach ($statement as $data) {
            $publi = new Publication();
            $publi->setIdPublication($data["idpublication"]);
            $publi->setMessage($data["message"]);
            $publi->setDate(new DateTime($data["date"]));
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($data["idutilisateur"]);
            $utilisateur->setLogin($data["login"]);
            $utilisateur->setNomPhotoDeProfil($data["nomphotodeprofil"]);
            $publi->setAuteur($utilisateur);
            $publis[] = $publi;
        }

        return $publis;
    }

    public function ajouter(Publication $publication)
    {
        $values = [
            "message" => $publication->getMessage(),
            "date" => $publication->getDate()->format('Y-m-d H:i:s'),
            "idAuteur" => $publication->getAuteur()->getIdUtilisateur()
        ];
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $statement = $pdo->prepare("INSERT INTO publications (message, date, idAuteur) VALUES(:message, :date, :idAuteur);");
        $statement->execute($values);
        return $pdo->lastInsertId();
    }

    public function recupererParClePrimaire($id) : ?Publication
    {
        $values = [
            "idpublication" => $id,
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT idpublication, message, date, idutilisateur, login, nomphotodeprofil
                                                FROM publications p
                                                JOIN utilisateurs u on p.idAuteur = u.idutilisateur
                                                WHERE idpublication = :idpublication");
        $statement->execute($values);
        $data = $statement->fetch();
        if ($data) {
            $publication = new Publication();
            $publication->setIdPublication($data["idpublication"]);
            $publication->setMessage($data["message"]);
            $publication->setDate(new DateTime($data["date"]));
            $utilisateur = new Utilisateur();
            $utilisateur->setIdUtilisateur($data["idutilisateur"]);
            $utilisateur->setLogin($data["login"]);
            $utilisateur->setNomPhotoDeProfil($data["nomphotodeprofil"]);
            $publication->setAuteur($utilisateur);
            return $publication;
        }
        return null;
    }

    public function mettreAJour(Publication $publication)
    {
        $values = [
            "idpublication" => $publication->getIdPublication(),
            "message" => $publication->getMessage(),
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("UPDATE publications SET message = :message WHERE idpublication = :idpublication;");
        $statement->execute($values);
    }

    public function supprimer(Publication $publication)
    {
        $values = [
            "idpublication" => $publication->getIdPublication(),
        ];
        $statement = ConnexionBaseDeDonnees::getPdo()->prepare("DELETE FROM publications WHERE idpublication = :idpublication");
        $statement->execute($values);
    }

}
