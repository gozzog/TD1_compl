<?php

namespace TheFeed\Modele\DataObject;

class Utilisateur
{


    private int $idutilisateur;
    private string $login;
    private string $mdphache;
    private string $email;
    private string $nomphotodeprofil;

    public function __construct()
    {
    }

    public static function construire(string $login,
                                      string $mdphache,
                                      string $email,
                                      string $nomphotodeprofil
    ): Utilisateur
    {
        $utilisateur = new Utilisateur();
        $utilisateur->setLogin($login);
        $utilisateur->setMdpHache($mdphache);
        $utilisateur->setEmail($email);
        $utilisateur->setNomPhotoDeProfil($nomphotodeprofil);
        return $utilisateur;
    }

    public function getIdUtilisateur(): int
    {
        return $this->idutilisateur;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function getMdpHache(): string
    {
        return $this->mdphache;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getNomPhotoDeProfil(): string
    {
        return $this->nomphotodeprofil;
    }

    public function setIdUtilisateur($idutilisateur): void
    {
        $this->idutilisateur = $idutilisateur;
    }

    public function setLogin($login): void
    {
        $this->login = $login;
    }

    public function setMdpHache($mdphache): void
    {
        $this->mdphache = $mdphache;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function setNomPhotoDeProfil($nomphotodeprofil): void
    {
        $this->nomphotodeprofil = $nomphotodeprofil;
    }
}
